import { Component } from '@angular/core';
import { NgForm } from '../../../node_modules/@angular/forms';
import { AuthService } from '../../services/auth';
import { LoadingController, AlertController } from '../../../node_modules/ionic-angular';

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {

  constructor(private authService: AuthService, private loadingCtrl : LoadingController,
  private alertCtrl: AlertController){}

  onSignin(form: NgForm){
    const loading = this.loadingCtrl.create({
      content: 'Signning you in...'
    });
    loading.present();
   this.authService.signin(form.value.email, form.value.password)
   .then(data => {
    loading.dismiss();
    })
   .catch(error =>{
     const alert = this.alertCtrl.create({
       title:'Signin failed!',
       message: error.message,
       buttons: ['Ok']
     });
     alert.present();
     loading.dismiss();
    });
  }
  

}
