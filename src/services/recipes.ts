import { Recipe } from "../models/recipe";
import { Ingredient } from "../models/ingredient";


export class RecipesService{

private recipes: Recipe[] = [];


addRecipe(title: String,
     description:String,
    difficulty: string, 
    ingredients : Ingredient[]){

    this.recipes.push(new Recipe(title,description,difficulty,ingredients));
    console.log(this.recipes);
    }
    

    getRecipes(){
    return this.recipes.slice();
    }

    removeRecipe(index: number){
     this.recipes.splice(index,1);
    }

    updateRecipes(index: number,
        title: String,
        description:String,
       difficulty: string, 
       ingredients : Ingredient[]){

        this.recipes[index] = new Recipe(title,description,difficulty,ingredients);
       }


}